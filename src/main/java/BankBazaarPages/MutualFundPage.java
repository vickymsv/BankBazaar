package BankBazaarPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler;


import wdMethods.SeMethods;

public class MutualFundPage extends SeMethods {
	String year,date, DOB;
	public MutualFundPage clickSearchFunds()  {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement eleclickSearch= locateElement("xpath", "//a[text()='Search for Mutual Funds']");
		click(eleclickSearch);
		return this;
	}

	public MutualFundPage dragAge() {
		Actions builder = new Actions(driver);	
		WebElement drag= locateElement("xpath", "//div[@class='rangeslider__handle']");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		builder.dragAndDropBy(drag,76, 0).perform();
		return this;
	}
	public MutualFundPage selectYear() {
		WebElement eleclickyear= locateElement("xpath", "//a[@data-value='1990-09']");
		year = eleclickyear.getText();
		System.out.println(year);
		click(eleclickyear);
		return this;
	}
	public MutualFundPage selectdate() {
		WebElement eleclickday= locateElement("xpath", "//div[@aria-label='day-12']");
		date = eleclickday.getText();
		System.out.println(date);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		click(eleclickday);
		DOB=(date+" "+year);
		return this;
	}
	public MutualFundPage verifybirthday() {
		WebElement eleYourBdy= locateElement("xpath", "(//span[@class='Calendar_highlight_xftqk'])[3]");

		verifyExactText(eleYourBdy, DOB);
		return this;
	}
	public MutualFundPage clickContinue() {		
		WebElement eleclickcontinue= locateElement("xpath", "//a[text()='Continue']");
		click(eleclickcontinue);
		return this;
	}

	public MutualFundPage selectsalary() throws InterruptedException {

		/*WebElement eleentersalary = locateElement("xpath","//input[@name='netAnnualIncome']");

		type(eleentersalary,"300000");

		Actions builder = new Actions(driver);	
		WebElement drag= locateElement("xpath", "//div[@class='rangeslider__handle']");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("none");
		}
		builder.dragAndDropBy(drag,58,0).perform();*/
		
		String salary = "670000";
		driver.findElementByName("netAnnualIncome").sendKeys(salary);

		Thread.sleep(3000);
		return this;
		
	
	}
	public EnterDetails selectbank() throws InterruptedException {
		
		WebElement elebank= locateElement("xpath","//span[text()='HDFC']");
		Thread.sleep(1000);
		click(elebank);
		
		return new EnterDetails();
	}
	

}
