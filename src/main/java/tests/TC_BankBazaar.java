package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import BankBazaarPages.LaunchBrowser;

import wdMethods.SeMethods;

public class TC_BankBazaar extends SeMethods {

	@BeforeClass
	public void setData() {
		testCaseName = "TC_Bankbazaar";
		testCaseDescription ="Check mutual fund";
		category = "Smoke";
		author= "vickyjaya";
		//dataSheetName="TCBANKBAZAAR";
}
	@Test
	public void bankbazaar() throws InterruptedException {
		new LaunchBrowser()
		.lbrowser()
		.clickMutualFund()
		.clickSearchFunds()
		.dragAge()
		.selectYear()
		.selectdate()
		.verifybirthday()
		.clickContinue()
		.selectsalary()
		.clickContinue()
		.selectbank()
		
		.enterfname()
		.viewMutualFund()
		.handlePopUp();
	}
	
}

